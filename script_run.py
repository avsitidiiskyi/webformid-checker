from core import *


driver = [None] * len(list_of_stage_urls)
for iterable, url in enumerate(list_of_stage_urls):
    email = name_for_email() + "@yopmail.com"
    options = Options()
    options.add_extension(mod_header)
    driver[iterable] = webdriver.Chrome(options=options, executable_path=path_to_driver)
    driver[iterable].get(f"https://webdriver.bewisse.com/add?X-Forwarded-For=77.222.144.250")
    driver[iterable].execute_script(f"window.open('{url}', 'new_window')")
    sleep(3)
    driver[iterable].switch_to.window(driver[iterable].window_handles[1])
    driver[iterable].maximize_window()
    sleep(7)
    pyautogui.press("f12")
    sleep(2)
    original_url = driver[iterable].current_url
    sign_in = driver[iterable].find_element_by_xpath("//span[contains(@class, 'log-In userName')]")
    sign_in.click()
    sleep(10)
    driver[iterable].switch_to.frame(driver[iterable].find_element_by_name("loginIframe"))
    sleep(3)
    driver[iterable].find_element_by_xpath("//a[contains(text(),'Sign Up')]").click()
    sleep(4)
    driver[iterable].find_element_by_xpath("//input[@id='email']").send_keys(email)
    sleep(1)
    driver[iterable].find_element_by_xpath("/html/body/div/div/app-root/div/ng-component/div/div/div/form/div[2]/div[3]/div[2]/button").click()
    sleep(3)
    driver[iterable].find_element_by_xpath("//input[@id='firstName']").send_keys("sd_test_name")
    sleep(0.5)
    driver[iterable].find_element_by_xpath("//input[@id='lastName']").send_keys("sd_test_surname")
    sleep(0.5)
    driver[iterable].find_element_by_xpath("//input[@id='pass']").send_keys("Tester12")
    sleep(0.5)
    driver[iterable].find_element_by_xpath("//input[@id='confirmPass']").send_keys("Tester12")
    sleep(0.5)
    submit = driver[iterable].find_element_by_xpath("//body/div[@id='wrapper']/div[1]/app-root[1]/div["
                                                    "1]/ng-component[1] "
                                                    "/div[1]/div[2]/form[1]/div[2]/div[6]/button[1]")
    submit.click()
    sleep(4)


    current_url = original_url.replace("/", "").replace(":", "").replace(".", "").replace("-", "")
    file_name = current_url + ".har"
    download_har_file(file_name)
    pyautogui.typewrite(current_url)
    sleep(1)
    pyautogui.press("enter")
    sleep(15)
    file_name = current_url + ".har"
    sleep(6)
    driver[iterable].quit()
    info = "".join(email)
    store_email_in_file([[info]])
    store_email_in_file(["".join(email)])

    try:
        get_actual_data(file_name)

        try:
            assert get_expected_data(file_name)["formId"] == get_actual_data(file_name)["formId"]
        except AssertionError:
            print(f"Form id for {url} is wrong")
        try:
            assert get_expected_data(file_name)["url"] == get_actual_data(file_name)["url"]
        except AssertionError:
            print(f"{url} is wrong")

        print(f"{WHITE}WebForm ID in {url} is {GREEN}present, {get_actual_data(file_name)['formId']}")

    except IndexError:
        print(f"{WHITE}WebForm ID in {url} is {RED}missing")
    info = "".join(email) + str(get_actual_data(file_name)["formId"])
    store_email_in_file([[info]])
