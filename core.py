import base64
from haralyzer import HarParser, HarPage
import json
from selenium import webdriver
import sys

from time import sleep
from selenium import webdriver
from selenium.webdriver.chrome.options import Options
import random
import csv
from string import ascii_letters
from random import choice
import transliterate
import pyautogui


def get_word_list_from_file(file_name):
    with open(file_name, "r", encoding='utf-8') as f:
        csv_reader = csv.reader(f)
        ruskie_list = [line[0] for line in csv_reader]
    return ruskie_list


def ruskie_name_male():
    return get_word_list_from_file("ruskie_name_male.csv")


def ruskie_name_FEmale():
    return get_word_list_from_file("ruskie_name_FEmale.csv")


def ruskie_surname_male():
    return get_word_list_from_file("ruskie_surname_male.csv")


def ruskie_surname_FEmale():
    return get_word_list_from_file("ruskie_surname_FEmale.csv")


def get_random_male_name():
    return random.choice(ruskie_name_male())


def get_random_FEmale_name():
    return random.choice(ruskie_name_FEmale())


def get_random_male_surname():
    return random.choice(ruskie_surname_male())


def get_random_FEmale_surname():
    return random.choice(ruskie_surname_FEmale())


def get_random_name():
    return random.choice([get_random_male_name(), get_random_FEmale_name()])


def get_random_surname_for_given_name(name):
    if name in ruskie_name_male():
        return get_random_male_surname()
    return get_random_FEmale_surname()


def magic():
    random_name = get_random_name()
    random_surname = get_random_surname_for_given_name(random_name)
    return random_name, random_surname


name, surname = magic()


def name_for_email():
    with open("names.csv", "r") as csv_file:
        csv_reader = csv.reader(csv_file)
        name_list = [line[0] for line in csv_reader]
    return random.choice(name_list) + str(random.randrange(90000))


def password():
    return (''.join(choice(ascii_letters) for word
                    in range(10))) + str(random.randrange(900))


def transliterator(arg):
    return transliterate.translit(arg, reversed=True)


path_to_folder = r"D:\Python\Python\WebformID"
RED = '\033[0;31m'
GREEN = '\033[0;36m'
WHITE = '\033[0;37m'

path_to_driver = r"D:\Python\exstensions\chromedriver.exe"
mod_header = r'D:\Python\extensions\modheader.crx'

list_of_stage_urls = ["https://www.sodapdf.com/pdf-merge/", "https://www.sodapdf.com/compress-pdf/",
                      "https://www.sodapdf.com/pdf-editor/", "https://www.sodapdf.com/pdf-converter/",
                      "https://www.sodapdf.com/split-pdf/", "https://www.sodapdf.com/ocr-pdf/",

                      "https://www.sodapdf.com/excel-to-pdf/",
                      "https://www.sodapdf.com/word-to-pdf/", "https://www.sodapdf.com/html-to-pdf/",
                      "https://www.sodapdf.com/jpg-to-pdf/", "https://www.sodapdf.com/png-to-pdf/",
                      "https://www.sodapdf.com/ppt-to-pdf/", "https://www.sodapdf.com/pdf-to-word/",
                      "https://www.sodapdf.com/txt-to-pdf/",
                      "https://www.sodapdf.com/gif-to-pdf/", "https://www.sodapdf.com/pdf-to-ppt/",
                      "https://www.sodapdf.com/docx-to-pdf/", "https://www.sodapdf.com/jpg-to-png/",
                      "https://www.sodapdf.com/tiff-to-pdf/", "https://www.sodapdf.com/pdf-to-excel/",
                      "https://www.sodapdf.com/pdf-to-jpg/", "https://www.sodapdf.com/pdf-to-html/",
                      "https://www.sodapdf.com/pdf-to-docx/", "https://www.sodapdf.com/png-to-jpg/",
                      "https://www.sodapdf.com/gif-to-png/", "https://www.sodapdf.com/bmp-to-jpg/",
                      "https://www.sodapdf.com/jpg-to-gif/", "https://www.sodapdf.com/gif-to-jpg/",
                      "https://www.sodapdf.com/delete-pdf-pages/", "https://www.sodapdf.com/rotate-pdf/",
                      "https://www.sodapdf.com/add-page-numbers-to-pdf/", "https://www.sodapdf.com/bates-numbering/",
                      "https://www.sodapdf.com/unlock-pdf/", "https://www.sodapdf.com/password-protect-pdf/",
                      "https://www.sodapdf.com/pdf-reader/", "https://www.sodapdf.com/pdf-form-filler-creator/",
                      "https://www.sodapdf.com/pdf-creator/"]


def get_expected_data(path_to_file):
    with open(path_to_file, 'r') as f:
        har_parsed = HarParser(json.loads(f.read()))
        string_container = []
        for element in str(har_parsed.har_data).rsplit('wfdata=', 1)[1]:
            string_container.append(element)
            if element == "'":
                break
        decode_text = ''.join(string_container)[:-1] \
            .replace('\n', '').replace('\r', '').replace('\t', '').replace(' ', '').replace('=', '').strip()
        missing_padding = len(decode_text) % 4
        if decode_text != 0:
            decode_text += str(b'=') * (4 - missing_padding)
        decode_text = str(base64.b64decode(decode_text))[2:]
        container = []
        for element in decode_text:
            container.append(element)
            if element == "}":
                break
        return json.loads(''.join(container))


def get_actual_data(second_har_file):
    with open(second_har_file, 'r') as f:
        har_parser = HarParser(json.loads(f.read()))
        counter = 0
        container = []
        for x in str(har_parser.har_data).rsplit('formId', 1)[1]:
            container.append(x)
            if x == '"':
                counter += 1
                if counter == 65:
                    break
        text_container = []
        counter = 0
        for symbol in '{"formId' + ''.join(container) + '}':
            text_container.append(symbol)
            if symbol == '"':
                counter += 1
                if counter == 20:
                    break
        return json.loads(''.join(text_container) + "}")


stored_password = ''.join([x for x in password()])


def download_har_file(file_name):
    pyautogui.moveTo(1671, 128)
    pyautogui.click()
    sleep(1)
    pyautogui.moveTo(1862, 154)
    pyautogui.click()
    sleep(2)
    pyautogui.moveTo(457, 435)
    sleep(1)
    pyautogui.click()
    pyautogui.press('backspace', presses=17)
    pyautogui.typewrite(path_to_folder)
    sleep(1)
    pyautogui.press("enter")
    sleep(1)
    pyautogui.typewrite(file_name)
    pyautogui.press("enter")
    sleep(1)
    pyautogui.click()


def store_email_in_file(document):
    text_container = f'output_emails.txt'
    with open(text_container, "a", newline='') as txt_file:
        writer = csv.writer(txt_file)
        for line in document:
            writer.writerow(line)
